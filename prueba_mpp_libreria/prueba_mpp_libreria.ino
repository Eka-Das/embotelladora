#include <AccelStepper.h>

AccelStepper stepper1(1, 27, 26);

//motor translado
//int DIR_traslado = 23; //define Direction pin
//int PUL_traslado = 24; //Pin para la señal de pulso
int EN_traslado = 25; //define Enable Pin

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  stepper1.setMaxSpeed(1000);
  stepper1.setSpeed(2000);
}

void loop() {
  // put your main code here, to run repeatedly:
  //digitalWrite(25, LOW);
  stepper1.setAcceleration(1000.0);
  stepper1.runToNewPosition(100);
  stepper1.setCurrentPosition(0);

  delay(750);
    //regreso
    stepper1.setAcceleration(1000.0);
    stepper1.runToNewPosition(80);
    stepper1.setCurrentPosition(0);

}
