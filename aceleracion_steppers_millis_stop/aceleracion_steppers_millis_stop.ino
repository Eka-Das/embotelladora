#include <AccelStepper.h>

//AccelStepper stepper(1, 27, 26);
AccelStepper stepper(1, 7, 6);
int stepper_estirado_enable_pin = 5;

int avance = 0;
int contadorInicial = 0;
int contadorFinal = 0;
int intervalo = 3000;
int paso = 0;

int induc_varilla_esti_abajo = 44;
int induc_varilla_esti_arriba = 47;


int valor_induc_varilla_esti_abajo = 0;
int valor_induc_varilla_esti_arriba = 0;

//int pin_pasos_stepper = 27;
//int pin_dir_stepper = 26;
int tiempo1 = 200;
int tiempo2 = 200;

int pin_entrada_pot = A3;
int lectura = 0;
int conversion = 0;

int x = 2;

void setup() {

  pinMode(pin_entrada_pot, INPUT);
  pinMode(stepper_estirado_enable_pin, OUTPUT );

  //  pinMode(pin_pasos_stepper, OUTPUT);
  // pinMode(pin_dir_stepper, OUTPUT);

  pinMode(9, OUTPUT);

  pinMode(induc_varilla_esti_abajo, INPUT);
  pinMode(induc_varilla_esti_arriba, INPUT);

  stepper.setMaxSpeed(5000.0);//pasos por segundo
  stepper.setAcceleration(5000);
  Serial.begin(9600);

}

void loop() {

  valor_induc_varilla_esti_abajo = digitalRead(induc_varilla_esti_abajo);
  valor_induc_varilla_esti_arriba = digitalRead(induc_varilla_esti_arriba);

  lectura = analogRead(pin_entrada_pot);
  conversion = map(lectura, 0, 1023, 0, 5000);

  tiempo1 = conversion;

  if (avance == 0) {
    digitalWrite(stepper_estirado_enable_pin, HIGH);
    stepper.moveTo(50000);
    stepper.run();
  }
  contadorFinal = millis();

  if (contadorFinal - contadorInicial > intervalo ) {
    avance = 1;
    stepper.stop();
    Serial.println("stop");
  }


}
