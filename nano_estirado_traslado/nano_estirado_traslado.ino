#include <AccelStepper.h>

//motores a pasos
AccelStepper stepper_estirado (1, 10, 11);
int stepper_estirado_enable_pin = 12;
int velocidad_estirado = 1200;

//Pines Entrada
int pin_entrada_subir = 6;
int pin_entrada_bajar = 5;

int valor_entrada_subir = 0;
int valor_entrada_bajar = 0;

//Control
int control_estirado = 1;
int subir = 1;
int bajar = 0;
int tiempo = 2000;
