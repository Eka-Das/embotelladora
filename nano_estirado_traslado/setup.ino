void setup() {
  //pines
  pinMode(pin_entrada_subir, INPUT);
  pinMode(pin_entrada_bajar, INPUT);

  //stepper estirado
  pinMode(stepper_estirado_enable_pin, OUTPUT);
  digitalWrite(stepper_estirado_enable_pin, HIGH);

  Serial.begin(9600);
  Serial.println("Nano");
}
