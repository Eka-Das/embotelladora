void estirado_baja() {
  //bajar

    if (valor_entrada_subir == LOW && valor_entrada_bajar == HIGH) {
      digitalWrite(stepper_estirado_enable_pin, HIGH);
      stepper_estirado.setMaxSpeed(1500.0);//pasos por segundo
      stepper_estirado.setSpeed(-velocidad_estirado);
      stepper_estirado.runSpeed();
      //Serial.println("bajar");
    }

    //detener abajo
    if (valor_entrada_subir == LOW && valor_entrada_bajar == LOW) {
      stepper_estirado.stop();
      stepper_estirado.setCurrentPosition(0);
      //Serial.println("detener abajo");
      bajar = 0;
      subir = 1;
      //delay(tiempo);
    }
  
}
