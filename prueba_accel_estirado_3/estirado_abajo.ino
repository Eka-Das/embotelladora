void estirado_abajo() {
  if (paso == 2) {
    if (valor_induc_varilla_esti_abajo == 1) {

      //Serial.println("bajar");

      digitalWrite(stepper_estirado_enable_pin, HIGH);

      //stepper_estirado.setAcceleration(10);
      //      stepper_estirado.setMaxSpeed(2000);
      //      stepper_estirado.setAcceleration(10);
      //stepper_estirado.moveTo(50000);
      stepper_estirado.setMaxSpeed(900.0);//pasos por segundo
      stepper_estirado.setSpeed(-velocidad_estirado);
      stepper_estirado.runSpeed();

      //Serial.println("valor_induc_varilla_esti_abajo == 1 && permiso_estirado_abajo == 1");
    }

    if (valor_induc_varilla_esti_abajo == 0) {
      permiso_estirado_abajo = false;
      stepper_estirado.stop();
      stepper_estirado.setCurrentPosition(0);

      //Serial.println("Estirado abajo");
      fin_ciclo_estirado = 1;
      permiso_acomodo_molde = 1;
      permiso_descompresion = 0;
      paso = 1;
      delay(1000);
    }
  }

}
