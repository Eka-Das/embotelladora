// ConstantSpeed.pde
// -*- mode: C++ -*-
//
// Shows how to run AccelStepper in the simplest,
// fixed speed mode with no accelerations
/// \author  Mike McCauley (mikem@airspayce.com)
// Copyright (C) 2009 Mike McCauley
// $Id: ConstantSpeed.pde,v 1.1 2011/01/05 01:51:01 mikem Exp mikem $

#include <AccelStepper.h>

//AccelStepper enablePin(28);
//AccelStepper stepper (1,28,26,27); // Defaults to AccelStepper::FULL4WIRE (4 pins) on 2, 3, 4, 5
AccelStepper stepper (1, 24, 23);
//AccelStepper stepper (1,5,6);
//5 pulso
//6 dir
//24 pul
//23 dir

int stepper_enable_pin = 25;


void setup()
{
  pinMode(stepper_enable_pin, OUTPUT );
  digitalWrite(stepper_enable_pin, LOW);
  stepper.setMaxSpeed(4000);
  stepper.setSpeed(-1500);
}

void loop()
{
  stepper.runSpeed();
}
