void modo_automatico() {
  //Serial.println("AUTOMATICO ACTIVO");
  if ( paso == 1) {            /*********Paso 1 in***********/
    //Serial.println("PASO 1");

    if (valor_sens_optico == 0 && lectura_optico == 0) { //detecta preforma
      soporte_trasero = 1;
      lectura_optico = 1;
      //Serial.println("lectura_optico = 1");
    }
    if (valor_sens_optico == 1 && lectura_optico == 0) { //detecta preforma
      soporte_trasero = 0;
      digitalWrite(rele_motor_molde, HIGH);
      lectura_optico = 1;
      //Serial.println("lectura_optico = 1 - soporte_trasero = 0");
    }
    traslado_final();

    if (fin_traslado_final == 1) {

      paso = 2;
      //Serial.println("paso 2");
    }
  }

  /*********Paso 1 END***********/

  if (paso == 2) {
    //    Serial.println("PASO 2");
    if (soporte_trasero == 1) {
      paso = 3;
      //Serial.println("paso 3");
    }
    if (soporte_trasero == 0) {
      regresa_traslado();

      digitalWrite(rele_motor_molde, HIGH); //APAGAR
      if (fin_regreso_traslado == 1) {
        lectura_optico = 0;
        paso = 1;
        //Serial.println("paso 1");
      }
    }
  }

  /*********Paso 3 in***********/
  if ( paso == 3 ) {
    //Serial.println("PASO 3");
    cierra_molde();

    if (fin_cierre_molde == 1) {
      fin_apertura_molde = 0;
      paso = 4;
      digitalWrite(pin_salida_a_nano_molde, HIGH);
      digitalWrite(pin_salida_a_nano_molde_ing_preforma, HIGH);
      //Serial.println("paso 4");
      avance = 0;
    }
  }
  /*********Paso 3 END***********/

  /*********Paso 4 in***********/
  if ( paso == 4 ) {
    if (paso4_serial == 0 ) {
      //Serial.println("paso 4");
      paso4_serial = 1;
    }
    estirado_arriba();

    if (fin_estirado_arriba == 1) {
      permiso_regreso_traslado = 0;
      digitalWrite(pin_salida_a_nano_molde, LOW);
      digitalWrite(pin_salida_a_nano_molde_ing_preforma  , LOW);
      paso = 5;
      //Serial.println("paso 5");
    }
  }
  /*********Paso 4 END***********/

  /*********Paso 5 in***********/
  if (paso == 5) {
    //    Serial.println("PASO 5");
    if (permiso_regreso_traslado == 0) {
      regresa_traslado();
      lectura_optico = 0;
    }
    
    if ( valor_induc_valv_descomp == 0 && permiso_descompresion == 1 ) {
      digitalWrite(rele_valvula_baja, HIGH);
      digitalWrite(rele_valvula_alta, HIGH);
      digitalWrite(rele_valvula_descompresion, LOW);
      fin_descompresion = 1;
    }
    
    if (fin_descompresion == 1) {
      inicio_varilla_abajo = 1;
      fin_descompresion = 0;//Correr mas abajo
    }
    
    if (inicio_varilla_abajo == 1) {
      digitalWrite(stepper_estirado_enable_pin, HIGH);
      stepper_estirado.setSpeed(-velocidad_estirado);
      stepper_estirado.runSpeed();
    }
    if (valor_induc_varilla_esti_abajo == 0) {
      fin_varilla_abajo = 1;
      inicio_varilla_abajo = 0;
      stepper_estirado.stop();
      digitalWrite(rele_valvula_descompresion, HIGH);
      //          Serial.println("stepper_estirado.stop");
    }
    if (valor_induc_molde_abierto == 0) {
      fin_apertura_molde = 1;
    }
    if (fin_varilla_abajo == 1 && fin_regreso_traslado == 1 && fin_apertura_molde == 1) {
      permiso_regreso_traslado = 0;
      fin_regreso_traslado = 0;
      fin_varilla_abajo = 0;
      fin_descompresion = 0;

      paso = 6;
      //Serial.println("lectura_optico = 0");
      //Serial.println("paso 6");
    }
  }
  /*********Paso 5 END***********/

  if (paso == 6) {
    //abrir molde
    //    Serial.println("PASO 6");
    digitalWrite(rele_motor_molde, LOW); //encender


    if (fin_apertura_molde == 1) {
      paso = 1;
      //Serial.println("paso 1");
    }
  }

}//void end
