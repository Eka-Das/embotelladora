
//sensores inductivos

int induc_varilla_esti_abajo = 44;
int induct_soplado_alta = 45;
int ind_soplado_baja = 46;
int induc_varilla_esti_arriba = 47;

int valor_induc_varilla_esti_abajo = 1;
int valor_induct_soplado_alta = 1;
int valor_induct_soplado_baja = 1;
int valor_induc_varilla_esti_arriba = 1;


//salidas señal a Nano
int pin_salida_subir = 32;
int pin_salida_bajar = 31;

//control
int control_estirado = 1;



void setup() {
  //inductivos
  pinMode(induc_varilla_esti_arriba, INPUT);
  pinMode(induc_varilla_esti_abajo, INPUT);
  pinMode(induct_soplado_alta, INPUT);
  pinMode(ind_soplado_baja, INPUT);

  //señal a arduino
  pinMode(pin_salida_subir, OUTPUT);
  pinMode(pin_salida_bajar, OUTPUT);


  Serial.begin(9600);
  Serial.print("RESELCO");
}

void loop() {

  leer_sensores();

  //subir
  if (control_estirado == 1) {
    if (valor_induc_varilla_esti_arriba == LOW) {
      digitalWrite(pin_salida_subir, HIGH);
      digitalWrite(pin_salida_bajar, LOW);
    }

    if (valor_induc_varilla_esti_arriba == 0) {
      digitalWrite(pin_salida_subir, HIGH);
      digitalWrite(pin_salida_bajar, HIGH);
    }
  }

  if (valor_induc_varilla_esti_arriba == 0) {
    digitalWrite(pin_salida_subir, HIGH);
    digitalWrite(pin_salida_bajar, HIGH);
  }


  //bajar
  if (control_estirado == 2) {
    if (valor_induc_varilla_esti_abajo != 0 ) {
      digitalWrite(pin_salida_subir, LOW);
      digitalWrite(pin_salida_bajar, HIGH);
      Serial.println("bajar");
    }
    //detener abajo
    if (valor_induc_varilla_esti_abajo == 0 ) {
      digitalWrite(pin_salida_subir, LOW);
      digitalWrite(pin_salida_bajar, LOW);
      Serial.println("detener abajo");

    }
  }

  if (valor_induc_varilla_esti_abajo == 0 ) {
    digitalWrite(pin_salida_subir, LOW);
    digitalWrite(pin_salida_bajar, LOW);
    Serial.println("detener abajo");

  }

}
