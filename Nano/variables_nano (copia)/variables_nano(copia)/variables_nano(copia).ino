#include <Servo.h>
#include <AccelStepper.h>

Servo servo_molde;
Servo servo_ingreso_preforma;
Servo servo_pinza_preforma;

AccelStepper mpap_pinza_molde(1, 5, 6);
AccelStepper mpap_pinza_ingreso_preforma(1, 7, 8);


int permiso_regreso_pinza;

int ctrl_activacion_ciclo_pinza_molde = 0; //permiso_ini
int ctrl_activacion_ciclo_pinza_in_preforma = 0;

int entrada_activ_pinza_molde=0;
int entrada_activ_pinza_ingr_pref=0;

//variables de tiempo
unsigned long tiempoI = 0;
unsigned long tiempoA = 0;
long tiempoR = 3000;

#define pin_in_ctrl_pinza_molde 11
#define pin_in_ctrl_pinza_ingr_pref 12

int pin_enable_mpap_molde = 0;
#define pin_enable_mpap_ingr_pref 10

int angulo_pinza_abierta = 45; //45
int angulo_pinza_cerrada= 135;//135

int tiempo_reinicia_variable_fin = 0;
int timpo_reinicia_variable_fin = 0;
long tempo_reinicia_variable_periodo = 5000;

bool permiso_activacion_pinza_inicio = true;
int servo_control = 0;

int fin_pap_ingreso_regreso = 0;
