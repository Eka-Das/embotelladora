estirado_arriba(){
  if (paso == 1)  {
    
    if (valor_induc_varilla_esti_arriba == 1) {     
      
      digitalWrite(stepper_estirado_enable_pin, HIGH);

      stepper_estirado.setMaxSpeed(1500.0);//pasos por segundo

      //stepper_estirado.setCurrentPosition(0);
      stepper_estirado.setAcceleration(10000);
      stepper_estirado.moveTo(2000);
      stepper_estirado.run();

      //stepper_estirado.setSpeed(velocidad_estirado-1000);
      //    stepper_estirado.runSpeed();
      //avance = 1;
    }


    if ( valor_induct_soplado_baja == 0) {
      digitalWrite(rele_valvula_baja, LOW);
    }

    if ( valor_induct_soplado_alta == 0) {
      stepper_estirado.setSpeed(100);
      stepper_estirado.runSpeed();
    }

    if (valor_induc_varilla_esti_arriba == 0) {
      fin_estirado_arriba = 1;
      //avance = 0;
      //Serial.println("detener");
      stepper_estirado.stop();
      permiso_estirado_abajo = 1;
      permiso_descompresion = 1;
      paso = 2;
      delay(1000);
    }

  }

}
