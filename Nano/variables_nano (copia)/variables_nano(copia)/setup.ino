void setup() {
  //mpap_pinza_moldes
  mpap_pinza_molde.setMaxSpeed(1000);
  mpap_pinza_ingreso_preforma.setMaxSpeed(1000);

  Serial.begin(9600);
  Serial.print("RESELCO");
  
  //Servos
  servo_molde.attach(2);
  servo_ingreso_preforma.attach(3);
  servo_pinza_preforma.attach(4);

  //mpap_pinza_moldes Config
  //mpap_pinza_molde molde
  mpap_pinza_molde.setMaxSpeed(1000);
  mpap_pinza_molde.setSpeed(2000);

  //mpap_pinza_molde Ingreso Preformas
  mpap_pinza_ingreso_preforma.setMaxSpeed(1000);
  mpap_pinza_ingreso_preforma.setSpeed(2000);

  //drivers Enable Control
  pinMode(pin_enable_mpap_molde, OUTPUT);
  pinMode(pin_enable_mpap_ingr_pref, OUTPUT);

  
}
