void pinza_in_pref() {

  if (ctrl_activacion_ciclo_pinza_in_preforma == 2) {

    servo_ingreso_preforma.write(angulo_pinza_abierta);
    //  servo_pinza_preforma.write(70);

    digitalWrite(pin_enable_mpap_ingr_pref, LOW); //low == energizado
    mpap_estirado.setAcceleration(1500.0);
    mpap_estirado.runToNewPosition(400);
    mpap_estirado.setCurrentPosition(0);
    digitalWrite(pin_enable_mpap_ingr_pref, HIGH); //HIGH
    fin_pap_ingreso_regreso = mpap_estirado.isRunning();
    if(fin_pap_ingreso_regreso == true ){
      
      
    }
    //delay(1000);

    //  servo_pinza_preforma.write(angulo_pinza_cerrada);
    //  servo_ingreso_preforma.write(angulo_pinza_cerrada);
    digitalWrite(pin_enable_mpap_ingr_pref, LOW); //low == energizado
    mpap_estirado.setAcceleration(1500.0);
    mpap_estirado.runToNewPosition(-400);
    mpap_estirado.setCurrentPosition(0);
    digitalWrite(pin_enable_mpap_ingr_pref, HIGH); //HIGH== desenergizado
    delay(1000);

    servo_ingreso_preforma.write(angulo_pinza_abierta);
    //activar desenergización mpap_traslado
    //digitalWrite(pin_enable_mpap_molde, LOW);

    ctrl_activacion_ciclo_pinza_in_preforma = 3;

  }
}
