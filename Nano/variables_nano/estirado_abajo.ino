void estirado_abajo() {

  if (valor_induc_varilla_esti_abajo == 1) {

    //Serial.println("bajar");

    digitalWrite(mpap_estirado_enable_pin, HIGH);

    //mpap_estirado.setAcceleration(10);
    //mpap_estirado.setMaxSpeed(2000);
    //mpap_estirado.setAcceleration(10);
    //mpap_estirado.moveTo(50000);
    
    mpap_estirado.setMaxSpeed(900.0);//pasos por segundo
    mpap_estirado.setSpeed(-velocidad_estirado);
    mpap_estirado.runSpeed();

    //Serial.println("valor_induc_varilla_esti_abajo == 1 && permiso_estirado_abajo == 1");
  }

  if (valor_induc_varilla_esti_abajo == 0) {
    permiso_estirado_abajo = false;
    mpap_estirado.stop();
    mpap_estirado.setCurrentPosition(0);

    //Serial.println("Estirado abajo");
    fin_ciclo_estirado = 1;
    permiso_acomodo_molde = 1;
    permiso_descompresion = 0;
  }


}
