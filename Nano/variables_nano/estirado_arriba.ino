void estirado_arriba() {

  if (valor_induc_varilla_esti_arriba == 1) {

    digitalWrite(mpap_estirado_enable_pin, HIGH);

    mpap_estirado.setMaxSpeed(1500.0);//pasos por segundo

    //mpap_estirado.setCurrentPosition(0);
    mpap_estirado.setAcceleration(10000);
    mpap_estirado.moveTo(2000);
    mpap_estirado.run();

    //mpap_estirado.setSpeed(velocidad_estirado-1000);
    //    mpap_estirado.runSpeed();
    //avance = 1;
  }

  if ( valor_induct_soplado_baja == 0) {
    digitalWrite(rele_valvula_baja, LOW);
  }

  if ( valor_induct_soplado_alta == 0) {
    mpap_estirado.setSpeed(100);
    mpap_estirado.runSpeed();
  }

  if (valor_induc_varilla_esti_arriba == 0) {

    fin_estirado_arriba = 1;
    //avance = 0;
    //Serial.println("detener");
    mpap_estirado.stop();
    permiso_estirado_abajo = 1;
    permiso_descompresion = 1;
    
  }

}
