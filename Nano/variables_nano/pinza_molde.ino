void pinza_molde() 

  if (ctrl_activacion_ciclo_pinza_molde == 1) {

    timpo_reinicia_variable_fin = millis();

    //activar desenergización mpap_traslado
    digitalWrite(pin_enable_mpap_molde, LOW);
    servo_molde.write(angulo_pinza_abierta);

    if (permiso_activacion_pinza_inicio == true) {
      //regreso
      Serial.println("regreso");
      mpap_traslado.setAcceleration(1000.0);
      mpap_traslado.runToNewPosition(80);
      mpap_traslado.setCurrentPosition(0);
      permiso_activacion_pinza_inicio = false;
    }

    angulo_pinza_cerrada;
    servo_molde.write(angulo_pinza_cerrada);

    tiempoI = millis();

    ctrl_activacion_ciclo_pinza_molde = 0;
    permiso_regreso_pinza = 0;

    //activar desenergización mpap_traslado
    digitalWrite(pin_enable_mpap_molde, HIGH);

  }

  tiempoA = millis();

  //Serial.println(tiempoR);
  if (((tiempoA - tiempoI) >= tiempoR) && permiso_regreso_pinza == 0) {

    digitalWrite(pin_enable_mpap_molde, LOW);//Energizar empap_traslado molde

    permiso_regreso_pinza = 1;
    Serial.println("avance");

    mpap_traslado.setAcceleration(1000.0);
    mpap_traslado.runToNewPosition(-80);
    mpap_traslado.setCurrentPosition(0);


    servo_molde.write(angulo_pinza_abierta);
    tiempoI = tiempoA + 2900;
    //activar desenergización mpap_traslado
    digitalWrite(pin_enable_mpap_molde, HIGH);

    delay(750);
    //regreso
    mpap_traslado.setAcceleration(1000.0);
    mpap_traslado.runToNewPosition(80);
    mpap_traslado.setCurrentPosition(0);



    tempo_reinicia_variable_periodo = 5000;

    //si no hay permiso y pasa un tiempo concede permiso de nuevo
    if (permiso_activacion_pinza_inicio == false &&
        timpo_reinicia_variable_fin - tiempo_reinicia_variable_fin >= tempo_reinicia_variable_periodo ) {
      permiso_activacion_pinza_inicio = true;
      tiempo_reinicia_variable_fin = timpo_reinicia_variable_fin;
    }


  }



}
