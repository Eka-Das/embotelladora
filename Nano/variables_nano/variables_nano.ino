#include <Servo.h>
#include <AccelStepper.h>

Servo servo_molde;
Servo servo_ingreso_preforma;
Servo servo_pinza_preforma;

AccelStepper mpap_traslado(1, 7, 8);
AccelStepper mpap_estirado(1, 10, 11);

int stepper_traslado_enable_pin = 12;
int mpap_estirado_enable_pin = 9;


int permiso_regreso_pinza;

int ctrl_activacion_ciclo_pinza_molde = 0; //permiso_ini
int ctrl_activacion_ciclo_pinza_in_preforma = 0;

int senal_estirado=0;
int senal_traslado=0;

//variables de tiempo
unsigned long tiempoI = 0;
unsigned long tiempoA = 0;
long tiempoR = 3000;

#define pin_in_ctrl_pinza_molde 5
#define pin_in_ctrl_pinza_ingr_pref 4

int pin_enable_mpap_molde = 0;
#define pin_enable_mpap_ingr_pref 10

int angulo_pinza_abierta = 45; //45
int angulo_pinza_cerrada= 135;//135

int tiempo_reinicia_variable_fin = 0;
int timpo_reinicia_variable_fin = 0;
long tempo_reinicia_variable_periodo = 5000;

bool permiso_activacion_pinza_inicio = true;
int servo_control = 0;

int fin_pap_ingreso_regreso = 0;
int fin_estirado_arriba = 1;

int valor_induc_varilla_esti_abajo = 1;
int valor_induct_soplado_alta = 1;
int valor_induct_soplado_baja = 1;
int valor_induc_varilla_esti_arriba = 1;

int velocidad_estirado = 1500;
int permiso_estirado_abajo = 1;
int permiso_descompresion = 1;
int fin_ciclo_estirado = 1;
int     permiso_acomodo_molde = 1;


#define rele_03 16
#define rele_04 17
#define rele_motor_molde 18
#define rele_valvula_baja 19
#define rele_valvula_descompresion 20
#define rele_valvula_alta 21


//pot para definir tiempo de espera de traslado en modo manual
int entrada_pot_pin_temporizador_traslado = A0;
int val_lectura_pot_temporizador_traslado;
int val_mapeado_pot_temporizador_traslado;

//motor translado
int DIR_traslado = 23; //define Direction pin
int PUL_traslado = 24; //Pin para la señal de pulso
int EN_traslado = 25; //define Enable Pin

//moto reductores - velocidades
int tiempo = 350; //600
int tiempo_espera = 350; //700

//moto reductor estirado
int tiempo_estirado = 300; //600
int tiempo_espera_estirado = 300; //

//estiradoestirado
int DIR_est = 26; //define Direction pin
int PUL_est = 27; //Pin para la señal de pulso
int EN_est = 28; //define Enable Pin
//motor estirado
int tiempo_est = 250; //250
int tiempo_espera_est = 250; //2000

//sensores inductores
int induc_valv_descomp = 38;
int induc_molde_cerrado = 39;
int induc_traslado_inicial = 40;
int induc_traslado_final = 41;
int induc_molde_abierto = 42;
int sens_optico = 43;
int induc_varilla_esti_abajo = 44;
int induct_soplado_alta = 45;
int ind_soplado_baja = 46;
int induc_varilla_esti_arriba = 47;

int led = 13;

int valor_induc_valv_descomp = 1;
int valor_induc_molde_cerrado = 1;
int valor_induc_traslado_inicial = 1;
int valor_induc_traslado_final = 1;
int valor_induc_molde_abierto = 1;
int valor_sens_optico = 1;





int rele_01 = 14;
int rele_02 = 15;

#define rele_03 16
#define rele_04 17
#define rele_motor_molde 18
#define rele_valvula_baja 19
#define rele_valvula_descompresion 20
#define rele_valvula_alta 21
int permiso = 0;

int x = 1; //contador de avance

//guarda el valor del sensor optico - si detectó  o no
int preforma;
int estirado;
int ctrl;//variable de control de estirado

int conteo;
int entrada;

int control;

int interruptor_pos_izquierda_auto;
int interruptor_pos_derecha_manual;
bool permiso_comprobacion_inicial = true; //true si le da permiso false si no

int fase_comprobacion[10];

//salidas señal a Nano
int pin_salida_a_nano_molde = 4; //11
int pin_salida_a_nano_molde_ing_preforma = 5; //12

//variables de control(permisos)
bool ctrl_traslado_pos_inicial_mod_manual = false; 

bool permiso_traslado_fin = true;
bool fin_estirado_abajo = true;


long crono_inicial_;
long crono_final;
long crono_periodo_transcurso_;

int modo_automatico_permiso = 0;
int modo_manual_permiso = 0;
int apagado = 0;

int tiempo_iniciado = 0;
int tiempo_inicial_espera_estirado = 0;
int tiempo_actual_espera_estirado = 0;
int paso = 1;
int acomodo_permiso = 0;
int lapso_varilla_arriba = 10;
int soporte_delantero = 0 ;
int soporte_trasero = 0;
int fin_regreso_traslado= 0;
int fin_traslado_final = 0;




int avance = 0;




int entrada_activ_pinza_molde=0;
int entrada_activ_pinza_ingr_pref=0;

//variables de tiempo




#define pin_in_ctrl_pinza_molde 11
#define pin_in_ctrl_pinza_ingr_pref 12

int enable_mpap_molde=0;
int enable_mpap_ingr_pref=0;



int acomodo_serial;
int paso4_serial = 0;
int paso3_serial = 0;
int paso5_serial = 0;
int paso2_serial = 0;
int paso1_serial = 0;
int permiso_serial_molde_abierto_1 = 0 ;
int serial_entro = 0;


int velocidad_traslado = 1500;
int vuelta_inicial = 0; //variable para que al inicio no cambie el valor de soporte delantero
int ctrl_estirado = 0; //sube y baja varilla estirado
int ctrl_delay = 0;
int ctrl_delay2 = 0;

int lectura_optico = 0;

int fin_cierre_molde = 0;
int fin_apertura_molde = 0;
int permiso_regreso_traslado = 0;
int fin_descompresion = 0;

int permiso_varilla_abajo=0;
int varilla_abajo=0;

int permiso_estirado_arriba = true;

int inicio_varilla_abajo = 0;
int fin_varilla_abajo = 0;
int paso_serial = 1;

//int fin_estirado_arriba = 1;

void setup() {
   pinMode (PUL_traslado, OUTPUT);
  pinMode (DIR_traslado, OUTPUT);
  pinMode (EN_traslado, OUTPUT);
  pinMode (PUL_est, OUTPUT);//estirado
  pinMode (DIR_est, OUTPUT);
  pinMode (EN_est, OUTPUT);
  pinMode (rele_01, OUTPUT);
  pinMode (rele_02, OUTPUT);
  pinMode (rele_03, OUTPUT);
  pinMode (rele_04, OUTPUT);
  pinMode (rele_motor_molde, OUTPUT);
  pinMode (rele_valvula_baja, OUTPUT);
  pinMode (rele_valvula_descompresion, OUTPUT);
  pinMode (rele_valvula_alta, OUTPUT);
  pinMode (led, OUTPUT);

  //señal a arduino
  pinMode(pin_salida_a_nano_molde, OUTPUT);
  pinMode(pin_salida_a_nano_molde_ing_preforma, OUTPUT);

  digitalWrite(led, LOW);
  digitalWrite(EN_traslado, LOW);
  digitalWrite(EN_est, LOW);
  digitalWrite(rele_01, HIGH);
  digitalWrite(rele_02, HIGH);
  digitalWrite(rele_03, HIGH);
  digitalWrite(rele_04, HIGH);
  digitalWrite(rele_motor_molde, HIGH);
  digitalWrite(rele_valvula_baja, HIGH);
  digitalWrite(rele_valvula_descompresion, HIGH);
  digitalWrite(rele_valvula_alta, HIGH);

  pinMode(induc_valv_descomp, INPUT);
  pinMode(induc_molde_cerrado, INPUT);
  pinMode(induc_traslado_inicial, INPUT);
  pinMode(induc_traslado_final, INPUT);
  pinMode(induc_molde_abierto, INPUT);
  pinMode(sens_optico, INPUT);
  pinMode(induc_varilla_esti_abajo, INPUT);
  pinMode(induct_soplado_alta, INPUT);
  pinMode(ind_soplado_baja, INPUT);
  pinMode(induc_varilla_esti_arriba, INPUT);

  //Steppers

  




  Serial.begin(9600);
  Serial.print("RESELCO");
  
  //Interruptor de inicio
  pinMode(5, INPUT);
  pinMode(4, INPUT);
  pinMode(stepper_traslado_enable_pin, OUTPUT );
  pinMode(mpap_estirado_enable_pin, OUTPUT );
  digitalWrite(stepper_traslado_enable_pin, LOW);
  digitalWrite(mpap_estirado_enable_pin, HIGH);
   
  //stepper_traslado.setSpeed(-2000);
  
