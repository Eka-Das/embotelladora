void setup() {
  pinMode (PUL_traslado, OUTPUT);
  pinMode (DIR_traslado, OUTPUT);
  pinMode (EN_traslado, OUTPUT);
  pinMode (PUL_est, OUTPUT);//estirado
  pinMode (DIR_est, OUTPUT);
  pinMode (EN_est, OUTPUT);
  pinMode (rele_01, OUTPUT);
  pinMode (rele_02, OUTPUT);
  pinMode (rele_03, OUTPUT);
  pinMode (rele_04, OUTPUT);
  pinMode (rele_motor_molde, OUTPUT);
  pinMode (rele_valvula_baja, OUTPUT);
  pinMode (rele_valvula_descompresion, OUTPUT);
  pinMode (rele_valvula_alta, OUTPUT);
  pinMode (led, OUTPUT);

  //señal a arduino
  pinMode(pin_salida_a_nano_molde, OUTPUT);
  pinMode(pin_salida_a_nano_molde_ing_preforma, OUTPUT);

  digitalWrite(led, LOW);
  digitalWrite(EN_traslado, LOW);
  digitalWrite(EN_est, LOW);
  digitalWrite(rele_01, HIGH);
  digitalWrite(rele_02, HIGH);
  digitalWrite(rele_03, HIGH);
  digitalWrite(rele_04, HIGH);
  digitalWrite(rele_motor_molde, HIGH);
  digitalWrite(rele_valvula_baja, HIGH);
  digitalWrite(rele_valvula_descompresion, HIGH);
  digitalWrite(rele_valvula_alta, HIGH);

  pinMode(induc_valv_descomp, INPUT);
  pinMode(induc_molde_cerrado, INPUT);
  pinMode(induc_traslado_inicial, INPUT);
  pinMode(induc_traslado_final, INPUT);
  pinMode(induc_molde_abierto, INPUT);
  pinMode(sens_optico, INPUT);
  pinMode(induc_varilla_esti_abajo, INPUT);
  pinMode(induct_soplado_alta, INPUT);
  pinMode(ind_soplado_baja, INPUT);
  pinMode(induc_varilla_esti_arriba, INPUT);

  //Steppers
  stepper_traslado.setMaxSpeed(3300);
  stepper_estirado.setMaxSpeed(3300);


  Serial.begin(9600);
  Serial.print("RESELCO");
  estirado;
  ctrl;



  //Interruptor de inicio
  pinMode(5, INPUT);
  pinMode(4, INPUT);
  /*
    myservo1.attach(8);  // attaches the servo on pin 9 to the servo object
    myservo2.attach(7);
    myservo3.attach(5);
  */
  pinMode(stepper_traslado_enable_pin, OUTPUT );
  pinMode(stepper_estirado_enable_pin, OUTPUT );
  digitalWrite(stepper_traslado_enable_pin, LOW);
  digitalWrite(stepper_estirado_enable_pin, HIGH);
  stepper_traslado.setSpeed(-2000);
  
  

  /*
    if(valor_sens_optico==0)Serial.println("foto inductor");//Foto inductor, detector de preforma
    if(valor_induct_soplado_alta==0)Serial.println("induct_soplado_alta");//soplado de alta
    if(valor_induct_soplado_baja==0)Serial.println("ind_soplado_baja"); //soplado de baja
    if(valor_induc_varilla_esti_arriba==0)Serial.println("induc_varilla_esti_arriba - Estirado arrriba");//tope final arriba varilla
  */
  /*
    if(valor_induc_valv_descomp==0)digitalWrite(rele_01,LOW);
    if(valor_induc_molde_cerrado==0)digitalWrite(rele_02,LOW);
    if(valor_induc_traslado_inicial==0)digitalWrite(rele_03,LOW);
    if(valor_induc_traslado_final==0)digitalWrite(rele_04,LOW);
    if(valor_induc_molde_abierto==0)digitalWrite(rele_motor_molde,LOW);
    if(valor_sens_optico==0)digitalWrite(rele_valvula_baja,LOW);
    if(valor_induc_varilla_esti_abajo==0)digitalWrite(rele_valvula_descompresion,LOW);
    if(valor_induct_soplado_alta==0)digitalWrite(rele_valvula_alta,LOW);
    if(valor_induct_soplado_baja==0)digitalWrite(rele_01,LOW);
    if(valor_induc_varilla_esti_arriba==0)digitalWrite(rele_02,LOW);

    if(valor_induc_valv_descomp==1)digitalWrite(rele_01,HIGH);
    if(valor_induc_molde_cerrado==1)digitalWrite(rele_02,HIGH);
    if(valor_induc_traslado_inicial==1)digitalWrite(rele_03,HIGH);
    if(valor_induc_traslado_final==1)digitalWrite(rele_04,HIGH);
    if(valor_induc_molde_abierto==1)digitalWrite(rele_motor_molde,HIGH);
    if(valor_sens_optico==1)digitalWrite(rele_valvula_baja,HIGH);
    if(valor_induc_varilla_esti_abajo==1)digitalWrite(rele_valvula_descompresion,HIGH);
    if(valor_induct_soplado_alta==1)digitalWrite(rele_valvula_alta,HIGH);
    if(valor_induct_soplado_baja==1)digitalWrite(rele_01,HIGH);
    if(valor_induc_varilla_esti_arriba==1)digitalWrite(rele_02,HIGH);
  */
  /*
    if(valor_ind_01==0)Serial.println("ind_01");//Descopreción valvula
    if(valor_induc_molde_cerrado==0)Serial.println("induc_molde_cerrado - Molde cerrado");//molde cerrado
    if(valor_ind_03==0)Serial.println("ind_03 - Traslado a la izquierda");//inductor translado posición de inicio de arrastre
    //if(valor_ind_03==0)Serial.println(valor_ind_03);
    if(valor_ind_04==0)Serial.println("ind_04");//Inductor translado posicón final arrastre
    if(valor_induc_molde_abierto==0)Serial.println("induc_molde_abierto");//Molde Abierto
    if(valor_ind_06==0)Serial.println("foto inductor");//Foto inductor, detector de preforma

    //if(valor_ind_06)Serial.println(valor_ind_06);
    if(valor_ind_07==0)Serial.println("ind_07");//paro barilla hasta abajo
    if(valor_ind_08==0)Serial.println("ind_08");//soplado de alta
    if(valor_induct_soplado_baja==0)Serial.println("ind_soplado_baja"); //soplado de baja
    if(valor_induc_varilla_esti_arriba==0)Serial.println("induc_varilla_esti_arriba - Estirado arrriba");//tope final arriba barilla
  */

}
