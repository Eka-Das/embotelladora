#include <AccelStepper.h>
#include <Servo.h>

AccelStepper stepper_traslado (1, 24, 23);
AccelStepper stepper_estirado (1, 27, 26);

AccelStepper stepper_pinza_molde(1, 5, 6);
AccelStepper stepper_pinza_preforma(1, 7, 8);

Servo myservo1;
Servo myservo2;
Servo myservo3;

int stepper_traslado_enable_pin = 25;
int stepper_estirado_enable_pin = 29;

//pot para definir tiempo de espera de traslado en modo manual
int entrada_pot_pin_temporizador_traslado = A0;
int val_lectura_pot_temporizador_traslado;
int val_mapeado_pot_temporizador_traslado;

//motor translado
int DIR_traslado = 23; //define Direction pin
int PUL_traslado = 24; //Pin para la señal de pulso
int EN_traslado = 25; //define Enable Pin

//moto reductores - velocidades
int tiempo = 350; //600
int tiempo_espera = 350; //700

//moto reductor estirado
int tiempo_estirado = 300; //600
int tiempo_espera_estirado = 300; //

//estiradoestirado
int DIR_est = 26; //define Direction pin
int PUL_est = 27; //Pin para la señal de pulso
int EN_est = 28; //define Enable Pin
//motor estirado
int tiempo_est = 250; //250
int tiempo_espera_est = 250; //2000

//sensores inductores
int induc_valv_descomp = 38;
int induc_molde_cerrado = 39;
int induc_traslado_inicial = 40;
int induc_traslado_final = 41;
int induc_molde_abierto = 42;
int sens_optico = 43;
int induc_varilla_esti_abajo = 44;
int induct_soplado_alta = 45;
int ind_soplado_baja = 46;
int induc_varilla_esti_arriba = 47;

int led = 13;

int valor_induc_valv_descomp = 1;
int valor_induc_molde_cerrado = 1;
int valor_induc_traslado_inicial = 1;
int valor_induc_traslado_final = 1;
int valor_induc_molde_abierto = 1;
int valor_sens_optico = 1;
int valor_induc_varilla_esti_abajo = 1;
int valor_induct_soplado_alta = 1;
int valor_induct_soplado_baja = 1;
int valor_induc_varilla_esti_arriba = 1;

int rele_01 = 14;
int rele_02 = 15;

#define rele_03 16
#define rele_04 17
#define rele_motor_molde 18
#define rele_valvula_baja 19
#define rele_valvula_descompresion 20
#define rele_valvula_alta 21
int permiso = 0;

int x = 1; //contador de avance

//guarda el valor del sensor optico - si detectó  o no
int preforma;
int estirado;
int ctrl;//variable de control de estirado

int conteo;
int entrada;

int control;

int interruptor_pos_izquierda_auto;
int interruptor_pos_derecha_manual;
bool permiso_comprobacion_inicial = true; //true si le da permiso false si no

int fase_comprobacion[10];

//salidas señal a Nano
int pin_salida_a_nano_molde = 4; //11
int pin_salida_a_nano_molde_ing_preforma = 5; //12

//variables de control(permisos)
bool ctrl_traslado_pos_inicial_mod_manual = false; 

bool permiso_traslado_fin = true;
bool fin_estirado_abajo = true;
bool fin_estirado_arriba = false;

long crono_inicial_;
long crono_final;
long crono_periodo_transcurso_;

int modo_automatico_permiso = 0;
int modo_manual_permiso = 0;
int apagado = 0;
int fin_ciclo_estirado = 0;
int tiempo_iniciado = 0;
int tiempo_inicial_espera_estirado = 0;
int tiempo_actual_espera_estirado = 0;
int paso = 0;
int acomodo_permiso = 0;
int lapso_varilla_arriba = 10;
int soporte_delantero = 0 ;
int soporte_trasero = 0;
int fin_regreso_traslado= 0;
int fin_traslado_final = 0;
int permiso_acomodo_molde  = 0;
int permiso_descompresion =1;

int permiso_regreso_pinza;

int ctrl_activacion_ciclo_pinza_molde = 0; //permiso_ini
int ctrl_activacion_ciclo_pinza_in_preforma = 0;

int entrada_activ_pinza_molde=0;
int entrada_activ_pinza_ingr_pref=0;

//variables de tiempo
unsigned long tiempoI = 0;
unsigned long tiempoA = 0;
long tiempoR = 3000;

#define pin_in_ctrl_pinza_molde 11
#define pin_in_ctrl_pinza_ingr_pref 12

int enable_mpap_molde=0;
int enable_mpap_ingr_pref=0;

int angulo_pinza_abierta = 45;
int angulo_pinza_cerrada= 135;

int tiempo_reinicia_variable_fin = 0;
int timpo_reinicia_variable_fin = 0;
long tempo_reinicia_variable_periodo = 5000;

bool permiso_activacion_pinza_inicio = true;
int acomodo_serial;
int paso4_serial = 0;
int paso3_serial = 0;
int paso5_serial = 0;
int paso2_serial = 0;
int paso1_serial = 0;
int permiso_serial_molde_abierto_1 = 0 ;
int serial_entro = 0;

int velocidad_estirado = 1500;
int vuelta_inicial = 0; //variable para que al inicio no cambie el valor de soporte delantero
int ctrl_estirado = 0; //sube y baja varilla estirado
int ctrl_delay = 0;
int ctrl_delay2 = 0;

int lectura_optico = 0;

int fin_cierre_molde = 0;
int fin_apertura_molde = 0;
int permiso_regreso_traslado = 0;
int fin_descompresion = 0;
