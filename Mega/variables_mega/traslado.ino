  void regresa_traslado() {
  //Serial.println("traslado 1");

  stepper_traslado.setSpeed(-2000);
  stepper_traslado.runSpeed();

  fin_regreso_traslado = 0;

  if (valor_induc_traslado_inicial == 0) {

    stepper_traslado.stop();
    fin_regreso_traslado = 1;
    permiso_regreso_traslado = 1;

  }

}

void traslado_final() {

  stepper_traslado.setSpeed(-2000); //cambiar valor por variable
  stepper_traslado.runSpeed();
  fin_traslado_final = 0;

  if (valor_induc_traslado_final == 0 ) {

    stepper_traslado.stop();
    fin_traslado_final = 1;

  }

}
