void modo_automatico() {

  if ( paso == 1) {            /*********Paso 1 in***********/

    if (valor_sens_optico == 0 && lectura_optico == 0) { //detecta preforma
      soporte_trasero = 1;
      lectura_optico = 1;
    }

    traslado_final();

    if (fin_traslado_final == 1) {
      paso = 2;
    }
  }

  /*********Paso 1 END***********/

  if (paso = 2) {
    if (soporte_trasero == 1) {
      paso = 3;
    }
    if (soporte_trasero == 0) {
      regresa_traslado();
      digitalWrite(rele_motor_molde, HIGH); //APAGAR
      if (fin_regreso_traslado == 1) {
        lectura_optico = 0;
        paso = 1;
      }
    }
  }

  /*********Paso 3 in***********/
  if ( paso == 3 ) {

    cierra_molde();

    if (fin_cierre_molde == 1) {
      paso = 4;
      digitalWrite(pin_salida_a_nano_molde, HIGH);
      digitalWrite(pin_salida_a_nano_ing_preforma , HIGH);
    }
  }
  /*********Paso 3 END***********/

  /*********Paso 4 in***********/
  if ( paso == 4 ) {
    estirado_arriba();
    if (fin_estirado_arriba == 1) {
      permiso_regreso_traslado = 0;
      digitalWrite(pin_salida_a_nano_molde, LOW);
      digitalWrite(pin_salida_a_nano_ing_preforma , LOW);
      paso = 5;
    }
  }
  /*********Paso 4 END***********/

  /*********Paso 5 in***********/
  if (paso == 5) {
    if (permiso_regreso_traslado == 0) {
      regresa_traslado();
    }
    if ( valor_induc_valv_descomp == 0 ) {
      fin_descompresion = 1;
    }
    if (fin_descompresion == 1) {
      inicio_varilla_abajo = 1;
    }
    if (inicio_varilla_abajo == 1) {
      digitalWrite(stepper_estirado_enable_pin, HIGH);
      stepper_estirado.setSpeed(velocidad_estirado);
      stepper_estirado.runSpeed();
    }
    if (valor_induc_varilla_esti_abajo == 0) {
      fin_varilla_abajo = 1;
      inicio_varilla_abajo = 0;
      stepper_estirado.stop();
    }
    if (fin_varilla_abajo == 1 && fin_regreso_traslado == 1) {
      permiso_regreso_traslado = 0;
      fin_regreso_traslado = 0;
      fin_varilla_abajo = 0;
      fin_descompresion = 0;

      paso = 6;
    }
  }
  /*********Paso 5 END***********/

  if (paso == 6) {
    //abrir molde
    digitalWrite(rele_motor_molde, LOW); //encender

    if (valor_induc_molde_abierto == 0) {

      fin_apertura_molde = 1;

    }
    if (fin_apertura_molde == 1) {
      paso = 1;
    }
  }

}//void end
