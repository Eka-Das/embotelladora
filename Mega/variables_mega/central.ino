void modo_apagado() {


  //detener todos los motores y servos
  digitalWrite(rele_motor_molde, HIGH); //apagar motor molde

  //desenergizar motores(hasta nuevo aviso)
  digitalWrite(stepper_traslado_enable_pin, LOW);
  digitalWrite(stepper_estirado_enable_pin, HIGH);

  fase_comprobacion[0] = false;
  fase_comprobacion[1] = false;
  fase_comprobacion[2] = false;
  fase_comprobacion[3] = false;
  fase_comprobacion[4] = false;

  permiso_comprobacion_inicial = true;
  vuelta_inicial=0;
}
