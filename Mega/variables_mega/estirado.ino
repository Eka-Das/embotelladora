void estirado_arriba() {
  fin_estirado_arriba = 0;

  digitalWrite(stepper_estirado_enable_pin, HIGH);
  stepper_estirado.setSpeed(-velocidad_estirado);
  stepper_estirado.runSpeed();

  if ( valor_induct_soplado_baja == 0) {
    digitalWrite(rele_valvula_baja, LOW);
  }

  if (valor_induct_soplado_alta == 0) {
    digitalWrite(rele_valvula_alta, LOW);
  }

  if (valor_induc_varilla_esti_arriba == 0) {
    fin_estirado_arriba = 1;
  }














    permiso_estirado_abajo = false;



    //activar válvulas
    if ( valor_induct_soplado_baja == 0) {
      digitalWrite(rele_valvula_baja, LOW);
    }

    if (valor_induct_soplado_alta == 0) {
      digitalWrite(rele_valvula_alta, LOW);
    }

  }

  if (valor_induc_varilla_esti_arriba == 0) {
  stepper_estirado.stop();
    //Serial.println("Estirado arriba");

    permiso_estirado_abajo = 1;

    permiso_descompresion = 1;
  }

  if ( permiso_descompresion == 1) {
  digitalWrite(rele_valvula_baja, HIGH);
    digitalWrite(rele_valvula_alta, HIGH);
    digitalWrite(rele_valvula_descompresion, LOW);
    //    permiso_estirado_abajo = true;
    //    permiso_estirado_arriba = false;

  }

}

void estirado_abajo() {

  if (valor_induc_varilla_esti_abajo == ! 0 && permiso_estirado_abajo == 1) {
    digitalWrite(stepper_estirado_enable_pin, HIGH);
    stepper_estirado.setSpeed(velocidad_estirado);
    stepper_estirado.runSpeed();
  }

  if (valor_induc_varilla_esti_abajo == 0) {
    permiso_estirado_abajo = false;
    stepper_estirado.stop();
    //Serial.println("Estirado abajo");
    fin_ciclo_estirado = 1;
    permiso_acomodo_molde = 1;
    permiso_descompresion = 0;
  }
}

void subir_estirado() {
  if (valor_induc_varilla_esti_arriba == ! 0 && ctrl_estirado == 0) {
    digitalWrite(stepper_estirado_enable_pin, LOW);
    stepper_estirado.setSpeed(-velocidad_estirado);
    stepper_estirado.runSpeed();
  } else {
    if (ctrl_delay == 0) {
      delay(500);
      ctrl_delay = 1;
      ctrl_delay2 = 0;
    }
    ctrl_estirado = 1;

  }

  if (valor_induc_varilla_esti_abajo == ! 0 && ctrl_estirado == 1) {
    digitalWrite(stepper_estirado_enable_pin, HIGH);
    stepper_estirado.setSpeed(velocidad_estirado);
    stepper_estirado.runSpeed();
  } else {
    if (ctrl_delay2 == 0) {
      delay(500);
      ctrl_delay2 = 1;
      ctrl_delay = 0;
    }
    ctrl_estirado = 0;

  }

}
