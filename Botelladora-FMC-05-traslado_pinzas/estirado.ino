void estirado_arriba() {

  fin_estirado_arriba = 0;

  if (valor_induc_varilla_esti_arriba == 1) {
    if (avance == 0) {
      digitalWrite(stepper_estirado_enable_pin, HIGH);
      //Serial.println("Subir");
      digitalWrite(pin_salida_nano_subir, HIGH);
      
      digitalWrite(pin_salida_nano_bajar, LOW);
      //delay(500);
    }
  }


  if ( valor_induct_soplado_baja == 0) {
    digitalWrite(rele_valvula_baja, LOW);
  }

  if ( valor_induct_soplado_alta == 0) {
    stepper_estirado.setSpeed(100);
    stepper_estirado.runSpeed();
  }

  if (valor_induc_varilla_esti_arriba == 0) {
    digitalWrite(pin_salida_nano_subir, HIGH);
    digitalWrite(pin_salida_nano_bajar, HIGH);
    
    fin_estirado_arriba = 1;
    avance = 1;
    permiso_estirado_abajo = 1;
    permiso_descompresion = 1;
  }
  permiso_estirado_abajo = false;


}

void estirado_abajo() {
  //Serial.println("estirado abajo");
  if (valor_induc_varilla_esti_abajo == 1 && permiso_estirado_abajo == 1) {
    
    digitalWrite(pin_salida_nano_subir, LOW);
    digitalWrite(pin_salida_nano_bajar, HIGH);

    //Serial.println("valor_induc_varilla_esti_abajo == 1 && permiso_estirado_abajo == 1");
  }

  if (valor_induc_varilla_esti_abajo == 0) {
    permiso_estirado_abajo = false;
    digitalWrite(pin_salida_nano_subir, LOW);
    digitalWrite(pin_salida_nano_bajar, LOW);

    //Serial.println("Estirado abajo");
    fin_ciclo_estirado = 1;
    permiso_acomodo_molde = 1;
    permiso_descompresion = 0;
  }
}

void subir_estirado() {
  if (valor_induc_varilla_esti_arriba == ! 0 && ctrl_estirado == 0) {

    digitalWrite(stepper_estirado_enable_pin, HIGH);
    stepper_estirado.setSpeed(velocidad_estirado);
    stepper_estirado.runSpeed();
  } else {
    if (ctrl_delay == 0) {
      delay(500);
      ctrl_delay = 1;
      ctrl_delay2 = 0;
    }
    ctrl_estirado = 1;

  }

  if (valor_induc_varilla_esti_abajo == ! 0 && ctrl_estirado == 1) {
    digitalWrite(stepper_estirado_enable_pin, HIGH);
    stepper_estirado.setSpeed(-velocidad_estirado);
    stepper_estirado.runSpeed();
  } else {
    if (ctrl_delay2 == 0) {
      delay(500);
      ctrl_delay2 = 1;
      ctrl_delay = 0;
    }
    ctrl_estirado = 0;

  }

}
