void estirado_arriba() {

  fin_estirado_arriba = 0;

  digitalWrite(stepper_estirado_enable_pin, HIGH);

  if (avance == 0) {
    digitalWrite(stepper_estirado_enable_pin, HIGH);
    stepper.moveTo(50000);
    stepper.run();
  }



  if ( valor_induct_soplado_baja == 0) {
    digitalWrite(rele_valvula_baja, LOW);
  }

  if (valor_induct_soplado_alta == 0) {
    digitalWrite(rele_valvula_alta, LOW);
  }

  if (valor_induc_varilla_esti_arriba == 0) {
    fin_estirado_arriba = 1;
  }
  permiso_estirado_abajo = false;

  if (valor_induc_varilla_esti_arriba == 0) {
    stepper_estirado.stop();
    //Serial.println("Estirado arriba");

    permiso_estirado_abajo = 1;
    permiso_descompresion = 1;
  }

}

void estirado_abajo() {
  //Serial.println("estirado abajo");
  if (valor_induc_varilla_esti_abajo == 1 && permiso_estirado_abajo == 1) {
    digitalWrite(stepper_estirado_enable_pin, HIGH);
    //stepper_estirado.setAcceleration(10);
    //      stepper_estirado.setMaxSpeed(2000);
    //      stepper_estirado.setAcceleration(10);
    //stepper_estirado.moveTo(50000);
    stepper_estirado.setSpeed(velocidad_estirado);
    stepper_estirado.runSpeed();
    //  Serial.println("valor_induc_varilla_esti_abajo == 1 && permiso_estirado_abajo == 1");
  }

  if (valor_induc_varilla_esti_abajo == 0) {
    permiso_estirado_abajo = false;
    stepper_estirado.stop();

    //Serial.println("Estirado abajo");
    fin_ciclo_estirado = 1;
    permiso_acomodo_molde = 1;
    permiso_descompresion = 0;
  }
}

void subir_estirado() {
  if (valor_induc_varilla_esti_arriba == ! 0 && ctrl_estirado == 0) {
    digitalWrite(stepper_estirado_enable_pin, HIGH);
    stepper_estirado.setSpeed(-velocidad_estirado);
    stepper_estirado.runSpeed();
  } else {
    if (ctrl_delay == 0) {
      delay(500);
      ctrl_delay = 1;
      ctrl_delay2 = 0;
    }
    ctrl_estirado = 1;

  }

  if (valor_induc_varilla_esti_abajo == ! 0 && ctrl_estirado == 1) {
    digitalWrite(stepper_estirado_enable_pin, HIGH);
    stepper_estirado.setSpeed(velocidad_estirado);
    stepper_estirado.runSpeed();
  } else {
    if (ctrl_delay2 == 0) {
      delay(500);
      ctrl_delay2 = 1;
      ctrl_delay = 0;
    }
    ctrl_estirado = 0;

  }

}
