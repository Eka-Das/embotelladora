void interruptor_arranque() {

  if ( interruptor_pos_izquierda_auto == HIGH) {
    modo_automatico_permiso = 1;
    modo_manual_permiso = 0;
    apagado = 0;
    }

  if ( interruptor_pos_derecha_manual == HIGH) {
    modo_automatico_permiso = 0;
    modo_manual_permiso = 1;
    apagado = 0;
  }

  //CENTRAL
  if (interruptor_pos_izquierda_auto == LOW && interruptor_pos_derecha_manual == LOW) {
    modo_automatico_permiso = 0;
    modo_manual_permiso = 0;
    apagado = 1;
    acomodo_permiso= 0;
    permiso_acomodo_molde = 0;
  }
}
