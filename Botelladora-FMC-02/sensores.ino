void leer_sensores(){
  
  //Int 1 a la derecha inicio normal
  //Int 2 a la izquierda inicio solo traslado
  interruptor_pos_derecha_manual = digitalRead(2);//manual
  interruptor_pos_izquierda_auto = digitalRead(3);//automatico


  valor_induc_molde_cerrado = digitalRead(induc_molde_cerrado);
  valor_induc_molde_abierto = digitalRead(induc_molde_abierto);

  valor_induc_traslado_inicial = digitalRead(induc_traslado_inicial);
  valor_induc_traslado_final = digitalRead(induc_traslado_final);
  valor_sens_optico = digitalRead(sens_optico);

  valor_induc_valv_descomp = digitalRead(induc_valv_descomp); //inferior, valor_induc_varilla_descomp,valor_induc_varilla_infe 

  if(valor_induc_valv_descomp == 0){
    permiso_descompresion = 1;
  }
  
  valor_induc_varilla_esti_abajo = digitalRead(induc_varilla_esti_abajo);
  valor_induct_soplado_alta = digitalRead(induct_soplado_alta);
  valor_induct_soplado_baja = digitalRead(ind_soplado_baja);

  valor_induc_varilla_esti_arriba = digitalRead(induc_varilla_esti_arriba);

  val_lectura_pot_temporizador_traslado = analogRead(entrada_pot_pin_temporizador_traslado); //
  val_mapeado_pot_temporizador_traslado = map(val_lectura_pot_temporizador_traslado, 0, 1023, 0, 3300);//map

  
}
