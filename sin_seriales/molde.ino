void cierra_molde() {
  if (digitalRead(induc_molde_cerrado) ==! 0) { //molde NO ESTÁ CERRADO
    digitalWrite(rele_motor_molde, LOW); //ENCENDER motor molde
  }else{
    digitalWrite(rele_motor_molde, HIGH); //apagar motor molde
  }
}

void abrir_molde() {
  if (digitalRead(induc_molde_abierto) ==! 0) { //si molde no está abierto
    digitalWrite(rele_motor_molde, LOW); //encender
  }else{
    digitalWrite(rele_motor_molde, HIGH); //APAGAR
    Serial.println("void abrir molde");
  }
}

void enciende_molde(){
  digitalWrite(rele_motor_molde, LOW); //encender
  
}

void apagar_molde(){
  digitalWrite(rele_motor_molde, HIGH); //APAGAR
}   
