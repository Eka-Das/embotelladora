void modo_automatico() {
  /*********Paso 1 in***********/
  if ( paso == 1) {
    if (paso1_serial == 0) {
      Serial.println("Paso 1");
      Serial.println("soporte_delantero");
      Serial.println(soporte_delantero);
      Serial.println("soporte_trasero");
      Serial.println(soporte_trasero);
      paso1_serial = 1;
      paso2_serial = 0;
    }
    traslado_final(); // -> asigna el paso 2
  }
  /*********Paso 1 END***********/

  /*********Paso 2 in***********/
  if ( paso == 2) { //comprobar preforma
    if (paso2_serial == 0) {
      Serial.println("Paso 2");
      Serial.println("soporte_delantero");
      Serial.println(soporte_delantero);
      Serial.println("soporte_trasero");
      Serial.println(soporte_trasero);
      paso2_serial = 1;
      paso1_serial = 0;
      paso3_serial = 0;
    }

    if ( fin_regreso_traslado == 1) {

      if (soporte_delantero == 1) {
        paso = 3;
      }
      if ( soporte_delantero == 0) {
        regresa_traslado();
        paso = 1;
      }

    }

  }
  /*********Paso 2 END***********/

  /*********Paso 3 in***********/
  if ( paso == 3 ) {
    if (paso3_serial == 0) {
      Serial.println("Paso 3");
      Serial.println("soporte_delantero");
      Serial.println(soporte_delantero);
      Serial.println("soporte_trasero");
      Serial.println(soporte_trasero);
      paso3_serial = 1;
      paso4_serial = 0;
      //fin_ciclo_estirado = 0;
      //permiso_estirado_arriba = true;
    }
    enciende_molde();

    if (valor_induc_molde_cerrado == 0) {
      paso = 4;
      permiso_estirado_arriba = 1;
      permiso_estirado_abajo = 0;
      fin_regreso_traslado = 0;
    }
  }
  /*********Paso 3 END***********/

  /*********Paso 4 in***********/
  if ( paso == 4 ) {
    if (paso4_serial == 0) {
      Serial.println("Paso 4");
      Serial.println("soporte_delantero");
      Serial.println(soporte_delantero);
      Serial.println("soporte_trasero");
      Serial.println(soporte_trasero);
      paso4_serial = 1;
      paso5_serial = 0;
    }
    //cerrar pinza
    estirado_arriba();



    if (permiso_estirado_arriba == 0 && permiso_estirado_abajo == 1) {
      regresa_traslado();
      estirado_abajo();

    }

    if ( fin_regreso_traslado == 1 && fin_ciclo_estirado == 1) {
      paso = 5;
    }
  }
  /*********Paso 4 END***********/

  /*********Paso 5 in***********/
  if (paso == 5) {
    if (paso5_serial == 0) {
      Serial.println("Paso 5");
      Serial.println("soporte_delantero");
      Serial.println(soporte_delantero);
      Serial.println("soporte_trasero");
      Serial.println(soporte_trasero);
      paso5_serial = 1;
    }

    if(){
      
    }

    if ( valor_induc_molde_abierto == 0 ) {
      if (permiso_serial_molde_abierto_1 == 0) {
        Serial.println("Se abrió molde");
      }

      //bajar_pinza();

      if ( soporte_delantero == 0) {
        apagar_molde();
        paso = 1;
      }
      if ( soporte_delantero ==  1) {
        paso = 3;
      }
    }
  }
  /*********Paso 5 END***********/

}//void end
